package com.example.final_project.repos;

import com.example.final_project.entities.Engine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EngineRepo extends JpaRepository<Engine, Long> {
    List<Engine> findByCarId(Long carId);
}