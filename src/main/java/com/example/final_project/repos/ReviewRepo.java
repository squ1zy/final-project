package com.example.final_project.repos;

import com.example.final_project.entities.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ReviewRepo extends JpaRepository<Review, Long> {
    List<Review> findByCarId(Long carId);
}