package com.example.final_project.repos;

import com.example.final_project.entities.Picture;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface PictureRepo extends JpaRepository<Picture, Long> {
    List<Picture> findByCarId(Long carId);
}
