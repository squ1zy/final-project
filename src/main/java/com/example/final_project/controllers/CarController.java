package com.example.final_project.controllers;


import com.example.final_project.entities.Car;
import com.example.final_project.entities.Engine;
import com.example.final_project.entities.Picture;
import com.example.final_project.entities.Review;
import com.example.final_project.repos.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api/cars")
@CrossOrigin(origins = "http://localhost:3000")
public class CarController {
    @Autowired
    private UserRepo userRepo;

    @Autowired
    private CarRepo carRepository;

    @Autowired
    private EngineRepo engineRepository;

    @Autowired
    private PictureRepo pictureRepository;

    @Autowired
    private ReviewRepo reviewRepository;

    @PostMapping("/add")
    public String addCar(
            @RequestParam("model") String model,
            @RequestParam("make") String make,
            @RequestParam("year") int year,
            @RequestParam("fuelConsumption") String fuelConsumption,
            @RequestParam("engineSize") String engineSize,
            @RequestParam("review") String reviewText,
            @RequestParam("reliability") double reviewScore,
            @RequestParam("turbo") boolean turbo,
            @RequestParam("kompressor") boolean kompressor,
            @RequestParam("supercharger") boolean supercharger,
            @RequestParam("carImage") MultipartFile carImage,
            @RequestParam("userId") Long userId) {

        try {
            Car car = new Car();
            car.setModel(model);
            car.setMake(make);
            car.setYear(year);
            car.setFuelConsumption(fuelConsumption);

            car.setUser(userRepo.findById(userId).orElseThrow(() -> new RuntimeException("User not found")));
            car = carRepository.save(car);

            Engine engine = new Engine();
            engine.setEngineSize(engineSize);
            engine.setCar(car);
            engineRepository.save(engine);

            Review review = new Review();
            review.setReviewText(reviewText);
            review.setReviewScore(reviewScore);
            review.setCar(car);
            reviewRepository.save(review);

            Picture picture = new Picture();
            picture.setPicture(carImage.getBytes());
            picture.setCar(car);
            pictureRepository.save(picture);

            return "Car added successfully!";
        } catch (IOException e) {
            e.printStackTrace();
            return "Error adding car";
        }
    }
}