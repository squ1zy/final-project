package com.example.final_project.controllers;

import com.example.final_project.Service.CarService;
import com.example.final_project.entities.Car;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("/api")
public class PostController {

    @Autowired
    private CarService carService;

    @GetMapping("/random-posts")
    public List<Car> getRandomPosts() {
        return carService.getAllCars();
    }

}