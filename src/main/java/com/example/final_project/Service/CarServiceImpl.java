package com.example.final_project.Service;

import com.example.final_project.entities.Car;
import com.example.final_project.repos.CarRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl extends CarService {

    @Autowired
    private CarRepo carRepository;

    public Car saveCar(Car car) {
        return carRepository.save(car);
    }
}
