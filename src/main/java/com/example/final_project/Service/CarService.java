package com.example.final_project.Service;

import com.example.final_project.entities.Car;
import com.example.final_project.repos.CarRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarService {

    @Autowired
    private CarRepo carRepository;

    public List<Car> getAllCars() {
        return carRepository.findAll();
    }



}
