import './App.css';
import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';
import AddPost from './components/AddPost';
import Feed from './components/Feed';
import Profile from './components/Profile';
import Login from './authentification/Login';
import Register from './authentification/Register';

function App() {
  return (
    <div className="App">
      <Router>
        <div>
          <nav className="navbar">
            <ul className="navbar-links">
              <li>
                <Link to="/addpost">Add Post</Link>
              </li>
              <li>
                <Link to="/feed">Feed</Link>
              </li>
              <li>
                <Link to="/profile">Profile</Link>
              </li>
              <li className='login'>
                <Link to="/login">Login</Link>
              </li>
              <li className='register'>
                <Link to="/register">Register</Link>
              </li>
            </ul>
          </nav>
          
          <Routes>
            <Route path="/feed" element={<Feed />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/addpost" element={<AddPost />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
          </Routes>
        </div>
      </Router>
    </div>
  );
}

export default App;