import React, { useState, useEffect } from 'react';
import './design/Feed.css';
import Post from './Post';
import axios from 'axios';

const Feed = () => {
    const [randomPosts, setRandomPosts] = useState([]);
    const [loading, setLoading] = useState(true);
    const [loggedInUsername, setLoggedInUsername] = useState('');

    useEffect(() => {
  
        const username = localStorage.getItem('username');
        setLoggedInUsername(username);

        const fetchRandomPosts = async () => {
            try {
                const response = await axios.get('http://localhost:8090/api/random-posts');
                setRandomPosts(response.data);
                console.log(response.data)
            } catch (error) {
                console.error('Error fetching random posts:', error);
            } finally {
                setLoading(false);
            }
        };

        fetchRandomPosts();
    }, []);

    return (
        <div className="feed-container">
            <div className="username-container">
                {loggedInUsername && (
                    <p className="username">Logged in as: {loggedInUsername}</p>
                )}
            </div>
            {loading ? (
                <p>Loading posts...</p>
            ) : (
                <div>
                    {randomPosts.length > 0 ? (
                        randomPosts.map(post => (
                            <div key={post.carId}>
                                <h2>{post.username}</h2>
                                <Post post={post} />
                            </div>
                        ))
                    ) : (
                        <p>No posts found.</p>
                    )}
                </div>
            )}
        </div>
    );
};

export default Feed;