import React, { useState } from 'react';
import './design/AddPost.css';
import axios from 'axios';

const AddPost = () => {
  const [post, setPost] = useState({
    model: '',
    make: '',
    year: '',
    fuelConsumption: '',
    engineSize: '',
    review: '',
    reliability: '',
    carImage: null,
    turbo: false,
    kompressor: false,
    supercharger: false
  });

  const handleChange = (e) => {
    const { name, value, type, checked } = e.target;
    setPost({
      ...post,
      [name]: type === 'checkbox' ? checked : value,
    });
  };

  const handleImageChange = (e) => {
    setPost({
      ...post,
      carImage: e.target.files[0], // Store the file object directly
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const formData = new FormData();
      formData.append('model', post.model);
      formData.append('make', post.make);
      formData.append('year', post.year);
      formData.append('fuelConsumption', post.fuelConsumption);
      formData.append('engineSize', post.engineSize);
      formData.append('review', post.review);
      formData.append('reliability', post.reliability);
      formData.append('carImage', post.carImage);
      formData.append('turbo', post.turbo);
      formData.append('kompressor', post.kompressor);
      formData.append('supercharger', post.supercharger);


      const userId = localStorage.getItem('userId');
      if (userId) {
        formData.append('userId', userId);
      } else {
        throw new Error('User ID not found');
      }

      await axios.post('http://localhost:8090/api/cars/add', formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      });
      setPost({
        model: '',
        make: '',
        year: '',
        fuelConsumption: '',
        engineSize: '',
        review: '',
        reliability: '',
        carImage: null,
        turbo: false,
        kompressor: false,
        supercharger: false
      });
      alert('Post submitted!');
    } catch (error) {
      console.error('Error adding post:', error);
      alert('Error submitting post. Please try again later.');
    }
  };

  return (
    <div className="container">
      <h1 className="title">Add Post</h1>
      <form className="form" onSubmit={handleSubmit}>
        <div className="form-group">
          <label>Car Model:</label>
          <input
            type="text"
            name="model"
            value={post.model}
            onChange={handleChange}
            placeholder="Enter car model"
            required
          />
        </div>
        <div className="form-group">
          <label>Car Make:</label>
          <input
            type="text"
            name="make"
            value={post.make}
            onChange={handleChange}
            placeholder="Enter car make"
            required
          />
        </div>
        <div className="form-group">
          <label>Manufacturing Year:</label>
          <input
            type="number"
            name="year"
            value={post.year}
            onChange={handleChange}
            placeholder="Enter manufacturing year"
            required
          />
        </div>
        <div className="form-group">
          <label>Fuel Consumption (L/100km):</label>
          <input
            type="number"
            step="0.1"
            name="fuelConsumption"
            value={post.fuelConsumption}
            onChange={handleChange}
            placeholder="Enter fuel consumption"
            required
          />
        </div>
        <div className="form-group">
          <label>Engine Size:</label>
          <input
            type="text"
            name="engineSize"
            value={post.engineSize}
            onChange={handleChange}
            placeholder="Enter engine size"
            required
          />
        </div>
        <div className="form-group checkbox-group">
          <label>
            <input
              type="checkbox"
              name="turbo"
              checked={post.turbo}
              onChange={handleChange}
            />
            Turbo
          </label>
          <label>
            <input
              type="checkbox"
              name="kompressor"
              checked={post.kompressor}
              onChange={handleChange}
            />
            Kompressor
          </label>
          <label>
            <input
              type="checkbox"
              name="supercharger"
              checked={post.supercharger}
              onChange={handleChange}
            />
            Supercharger
          </label>
        </div>
        <div className="form-group">
          <label>Review Text:</label>
          <textarea
            name="review"
            value={post.review}
            onChange={handleChange}
            placeholder="Enter your review"
            required
          ></textarea>
        </div>
        <div className="form-group">
          <label>Reliability (out of 5):</label>
          <input
            type="number"
            min="1"
            max="5"
            name="reliability"
            value={post.reliability}
            onChange={handleChange}
            placeholder="Enter reliability rating"
            required
          />
        </div>
        <div className="form-group">
          <label>Upload Car Image:</label>
          <input
            type="file"
            accept="image/*"
            onChange={handleImageChange}
            required
          />
        </div>
        {post.carImage && (
          <div className="image-preview">
            <img src={URL.createObjectURL(post.carImage)} alt="Car Preview" />
          </div>
        )}
        <button type="submit" className="btn">Submit Post</button>
      </form>
    </div>
  );
}

export default AddPost;