import React from 'react';
import './design/Post.css';

const Post = ({ post }) => {
  return (
    <div className="post-container">
      <div className="post-header">
        <h2>{post.user.username}</h2>
      </div>
      <div className="post-content">
        <div className="post-detail">
          <strong>Make:</strong> {post.make}
        </div>
        <div className="post-detail">
          <strong>Model:</strong> {post.model}
        </div>
        <div className="post-detail">
          <strong>Year:</strong> {post.year}
        </div>
        <div className="post-detail">
          <strong>Fuel Consumption:</strong> {post.fuelConsumption}
        </div>
      </div>
    </div>
  );
};

export default Post;